function* testsaga(){
  try {
    yield put({type:'FETCH_PRODUCT'})
    const data = yield call(loadData)
    yield put(
          {
            type:'FETCH_PRODUCT',
            payload:data
          })
  } catch (e) {
      yield put({
        type:'FETCH_PRODUCT',
        payload:e
      })
  } finally {
      console.log(data)
  }
}
