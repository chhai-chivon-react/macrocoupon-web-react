import React from 'react';
import { Component } from 'react';

import { Grid, Row, Col} from 'react-bootstrap';
import { Button } from 'react-bootstrap';


//import css
import css from '../../../styles/style.css';


class Body extends Component{
  render(){
    return(
      <div className="container">
      <div className="row">
            <div className="col-sm-6 col-md-3 col-xs-12">
              <div className="thumbnail">
                <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                <div className="caption">
                  <h3>Product Name</h3>
                  <p> Product dscription </p>
                    <p>
                        <a href="#" className="btn btn-primary" role="button">Button</a>
                        <a href="#" className="btn btn-default" role="button">Button</a>
                    </p>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-3 col-xs-12">
              <div className="thumbnail">
                <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                <div className="caption">
                  <h3>Product Name</h3>
                  <p> Product dscription </p>
                    <p>
                        <a href="#" className="btn btn-primary" role="button">Button</a>
                        <a href="#" className="btn btn-default" role="button">Button</a>
                    </p>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-3 col-xs-12">
              <div className="thumbnail">
                <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                <div className="caption">
                  <h3>Product Name</h3>
                  <p> Product dscription </p>
                    <p>
                        <a href="#" className="btn btn-primary" role="button">Button</a>
                        <a href="#" className="btn btn-default" role="button">Button</a>
                    </p>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-3 col-xs-12">
              <div className="thumbnail">
                <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                <div className="caption">
                  <h3>Product Name</h3>
                  <p> Product dscription </p>
                    <p>
                        <a href="#" className="btn btn-primary" role="button">Button</a>
                        <a href="#" className="btn btn-default" role="button">Button</a>
                    </p>
                </div>
              </div>
            </div>
        </div>
        <div className="row">
              <div className="col-sm-6 col-md-3 col-xs-12">
                <div className="thumbnail">
                  <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                  <div className="caption">
                    <h3>Product Name</h3>
                    <p> Product dscription </p>
                      <p>
                          <a href="#" className="btn btn-primary" role="button">Button</a>
                          <a href="#" className="btn btn-default" role="button">Button</a>
                      </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-3 col-xs-12">
                <div className="thumbnail">
                  <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                  <div className="caption">
                    <h3>Product Name</h3>
                    <p> Product dscription </p>
                      <p>
                          <a href="#" className="btn btn-primary" role="button">Button</a>
                          <a href="#" className="btn btn-default" role="button">Button</a>
                      </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-3 col-xs-12">
                <div className="thumbnail">
                  <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                  <div className="caption">
                    <h3>Product Name</h3>
                    <p> Product dscription </p>
                      <p>
                          <a href="#" className="btn btn-primary" role="button">Button</a>
                          <a href="#" className="btn btn-default" role="button">Button</a>
                      </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-3 col-xs-12">
                <div className="thumbnail">
                  <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                  <div className="caption">
                    <h3>Product Name</h3>
                    <p> Product dscription </p>
                      <p>
                          <a href="#" className="btn btn-primary" role="button">Button</a>
                          <a href="#" className="btn btn-default" role="button">Button</a>
                      </p>
                  </div>
                </div>
              </div>
          </div>
            <div className="row">
                  <div className="col-sm-6 col-md-3 col-xs-12">
                    <div className="thumbnail">
                      <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                      <div className="caption">
                        <h3>Product Name</h3>
                        <p> Product dscription </p>
                          <p>
                              <a href="#" className="btn btn-primary" role="button">Button</a>
                              <a href="#" className="btn btn-default" role="button">Button</a>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-xs-12">
                    <div className="thumbnail">
                      <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                      <div className="caption">
                        <h3>Product Name</h3>
                        <p> Product dscription </p>
                          <p>
                              <a href="#" className="btn btn-primary" role="button">Button</a>
                              <a href="#" className="btn btn-default" role="button">Button</a>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-xs-12">
                    <div className="thumbnail">
                      <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                      <div className="caption">
                        <h3>Product Name</h3>
                        <p> Product dscription </p>
                          <p>
                              <a href="#" className="btn btn-primary" role="button">Button</a>
                              <a href="#" className="btn btn-default" role="button">Button</a>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-xs-12">
                    <div className="thumbnail">
                      <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                      <div className="caption">
                        <h3>Product Name</h3>
                        <p> Product dscription </p>
                          <p>
                              <a href="#" className="btn btn-primary" role="button">Button</a>
                              <a href="#" className="btn btn-default" role="button">Button</a>
                          </p>
                      </div>
                    </div>
                  </div>
              </div>
              <div className="row">
                    <div className="col-sm-6 col-md-3 col-xs-12">
                      <div className="thumbnail">
                        <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                        <div className="caption">
                          <h3>Product Name</h3>
                          <p> Product dscription </p>
                            <p>
                                <a href="#" className="btn btn-primary" role="button">Button</a>
                                <a href="#" className="btn btn-default" role="button">Button</a>
                            </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-xs-12">
                      <div className="thumbnail">
                        <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                        <div className="caption">
                          <h3>Product Name</h3>
                          <p> Product dscription </p>
                            <p>
                                <a href="#" className="btn btn-primary" role="button">Button</a>
                                <a href="#" className="btn btn-default" role="button">Button</a>
                            </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-xs-12">
                      <div className="thumbnail">
                        <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                        <div className="caption">
                          <h3>Product Name</h3>
                          <p> Product dscription </p>
                            <p>
                                <a href="#" className="btn btn-primary" role="button">Button</a>
                                <a href="#" className="btn btn-default" role="button">Button</a>
                            </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-xs-12">
                      <div className="thumbnail">
                        <img className="img-responsive" src="https://img.grouponcdn.com/deal/ieF2u2dwaUkkSSRyVRZaCBGA588/ie-1000x600/v1/c440x266q85.jpg" />
                        <div className="caption">
                          <h3>Product Name</h3>
                          <p> Product dscription </p>
                            <p>
                                <a href="#" className="btn btn-primary" role="button">Button</a>
                                <a href="#" className="btn btn-default" role="button">Button</a>
                            </p>
                        </div>
                      </div>
                    </div>
                </div>
      </div>
    );
  }
}
export default Body;
