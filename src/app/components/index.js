import React from 'react';
import { Component } from 'react';
import { Button  } from 'react-bootstrap';

import { Link } from 'react-router';

import HeaderApp from './header/header';
import BodyApp from './body/body';
import FooterApp from './footer/footer';
const Index = React.createClass({
  render(){
    return(
      <div>
          <HeaderApp />
          <BodyApp />
          <FooterApp />
      </div>
    );
  }
});
export default Index;
