import React from 'react';
import { Component } from 'react';
import { Button  } from 'react-bootstrap';

import { Link } from 'react-router';

const ProductGrid = React.createClass({
  render(){
    return(
      <div className="product-grid">
        <h1>
          <Link to="/">ProductGrid</Link>
        </h1>
      </div>
    )
  }
});

export default ProductGrid;
