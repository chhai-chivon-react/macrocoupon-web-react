import React from 'react';
import { Component } from 'react';
import { Button  } from 'react-bootstrap';

import { Link } from 'react-router';

const ProductView = React.createClass({
  render(){
    return(
      <div className="product-view">
        <h1>
          <Link to="/">ProductView</Link>
        </h1>
      </div>
    )
  }
});
export default ProductView;
