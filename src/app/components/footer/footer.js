import React from 'react';
import { Component } from 'react';


//import component
import AboutApp from './about';
import SocialApp from './social';

//import css
import css from '../../../styles/style.css';
class Footer extends Component{
  render(){
    return(
        <div className="footer-container">
            <div>
                <AboutApp />
            </div>
            <div>
                <SocialApp />
            </div>
            <div className="footer">
              <p>
                ​© 2016 Macrocoupon,Inc. All Rights Reserved.
              </p>
            </div>
        </div>
    );
  }
}
export default Footer;
