import { createStore, Component } from 'react';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';


//import the root reducer
import rootReducer from './reducers/index';

import comment from './data/comment';
import post from './data/post';


//create an object for the default data
const defaultState ={
  post,
  comment
}

export const store = createStore( rootReducer,defaultState);

export const history = syncHistoryWithStore(browserHistory, store);

export default store;
