import _ from 'lodash';
import { SELECT_PRODUCT } from '../actions/types';


const PRODUCTS = [
  { title: 'Product1', price: 1000 },
  { title: 'Product2', price: 100 },
  { title: 'Product3', price: 100 },
  { title: 'Product4', price: 100 }
];
