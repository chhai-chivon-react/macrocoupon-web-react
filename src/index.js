import React from 'react';
import ReactDOM from 'react-dom';

//Import css
import css from './styles/style.css';

//Import components
import App from './app/components/index';
import Product from './app/components/products';
import ProductView from './app/components/product-view';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

//import App from './app/components/app';
import reducers from './app/reducers';

import TEST from './app/containers/book-list';

//Import react router
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

const router =(
  <Router history={browserHistory}>
    <Route path="/" component={TEST}>
      <IndexRoute component={ProductView}></IndexRoute>
      <Route path="/product/:productId" component={ProductView}></Route>
    </Route>
  </Router>
)
ReactDOM.render(
    <Provider store={createStore(reducers)}>
      <TEST />
    </Provider>
  ,document.getElementById('app'));
